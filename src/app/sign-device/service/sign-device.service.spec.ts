import { TestBed } from '@angular/core/testing';

import { SignDeviceService } from './sign-device.service';

describe('SignDeviceService', () => {
  let service: SignDeviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SignDeviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
