import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { FcmService } from 'src/app/services/fcm.service';

import { ActionPerformed, PushNotifications, PushNotificationSchema } from '@capacitor/push-notifications';
import { runInThisContext } from 'vm';


@Component({
  selector: 'app-sign-device',
  templateUrl: './sign-device.page.html',
  styleUrls: ['./sign-device.page.scss'],
})
export class SignDevicePage implements OnInit {

  deviceToken: string = "";

  notification: PushNotificationSchema = null;
  
  title: string = null;
  body: string = null;
  data: string = null;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private fcm: FcmService,
    private zone: NgZone
    ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.notification = this.router.getCurrentNavigation().extras.state.notification;
      }
    });
  }

  ngOnInit() {
    this.deviceToken = localStorage.getItem('deviceToken');

    if (this.notification) {
      this.title = this.notification.title;
      this.body = this.notification.body;
      this.data = JSON.stringify(this.notification.data);
    }
    
    
    PushNotifications.addListener(
      'pushNotificationReceived',
      async (notification:PushNotificationSchema) => {
        this.zone.run(() => {
          this.title = notification.title;
          this.body = notification.body;
          this.data = JSON.stringify(notification.data);
        });
      }
    )

    console.log(this.notification);
  }

  async registerDevice(){
      this.fcm.initPush();

      const delay = ms => new Promise(res => setTimeout(res, ms));
      await delay(2000);

      this.deviceToken = localStorage.getItem('deviceToken');
  }

}