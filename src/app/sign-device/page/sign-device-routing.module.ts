import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignDevicePage } from './sign-device.page';

const routes: Routes = [
  {
    path: '',
    component: SignDevicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignDevicePageRoutingModule {}
