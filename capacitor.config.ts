import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'br.com.klupp.notification.test',
  appName: 'FCM Push Tester',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
