import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { ActionPerformed, PushNotifications, PushNotificationSchema, Token } from '@capacitor/push-notifications';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class FcmService {
  
  private deviceToken: string;

  constructor(
    private router: Router,
    private configurationService: ConfigurationService
  ) { }


  public initPush() : string{
    if (Capacitor.getPlatform() !== 'web'){
      this.registerPush();
      return this.deviceToken;
    }

  }

  private registerPush(){
    PushNotifications.requestPermissions()
      .then(async (permission) => {
        if(permission.receive == "granted")
          await PushNotifications.register();
      })
    
    PushNotifications.addListener(
      'registration',
      (token: Token) => {
        console.debug(token.value);
        
        this.configurationService.saveConfig('deviceToken', token.value);

        this.deviceToken = token.value;
      }
    )

    PushNotifications.addListener(
      'registrationError',
      (error: any) => {
        console.debug(error);
      } 
    )

    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      async (notification: ActionPerformed) => {
        let navigationExtras: NavigationExtras = {
          state: {
            notification: notification.notification as PushNotificationSchema
          }
        };

        this.router.navigateByUrl(`/sign-device`, navigationExtras);
      }
    )
  }
}
