import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignDevicePageRoutingModule } from './sign-device-routing.module';

import { SignDevicePage } from './sign-device.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignDevicePageRoutingModule
  ],
  declarations: [SignDevicePage]
})
export class SignDevicePageModule {}
