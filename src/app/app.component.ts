import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Sign Device', url: `/sign-device`, icon: 'phone-portrait' },
  ];
  
  constructor() {}
}
