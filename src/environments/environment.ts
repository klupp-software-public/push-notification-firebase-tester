// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB58gVJXR_rJHLyOiP0O0WkXnciLTlUFaU",
    authDomain: "animai.firebaseapp.com",
    projectId: "animai",
    storageBucket: "animai.appspot.com",
    messagingSenderId: "206549421687",
    appId: "1:206549421687:web:9ee106ab38ea6e8d128072",
    measurementId: "G-ZCZBBN2PZ1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
