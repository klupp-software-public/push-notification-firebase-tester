import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor() { }

  saveConfig(key: string, value: string){
    localStorage.setItem(key, value);
  }
}
